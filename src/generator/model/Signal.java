package generator.model;

public class Signal {
	
	String name;
	double value;
	SignalType type;
	SignalAtr atributte;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public SignalType getType() {
		return type;
	}
	public void setType(SignalType type) {
		this.type = type;
	}
	public SignalAtr getAtributte() {
		return atributte;
	}
	public void setAtributte(SignalAtr atributte) {
		this.atributte = atributte;
	}
}

