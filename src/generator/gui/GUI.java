package generator.gui;

import generator.model.Atributes;
import generator.model.Signal;
import generator.model.SignalType;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import javax.swing.JTextPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;

public class GUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 200, 902, 504);
		setTitle("AWF file editor" );
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmOpenFile = new JMenuItem("Open file");
		mnNewMenu.add(mntmOpenFile);
		
		JMenuItem mntmExportToVhdl = new JMenuItem("Export to VHDL");
		mnNewMenu.add(mntmExportToVhdl);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Save as XML");
		mnNewMenu.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JTree tree = new JTree();
		tree.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("File name") {
				{
					DefaultMutableTreeNode node_1;
					node_1 = new DefaultMutableTreeNode("Signals1");
						node_1.add(new DefaultMutableTreeNode("s1"));
						node_1.add(new DefaultMutableTreeNode("s2"));
						node_1.add(new DefaultMutableTreeNode("s3"));
						node_1.add(new DefaultMutableTreeNode("s4"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("Signals2");
						node_1.add(new DefaultMutableTreeNode("s1"));
						node_1.add(new DefaultMutableTreeNode("s2"));
						node_1.add(new DefaultMutableTreeNode("s3"));
						node_1.add(new DefaultMutableTreeNode("s4"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("Singnals3");
						node_1.add(new DefaultMutableTreeNode("s1"));
						node_1.add(new DefaultMutableTreeNode("s2"));
						node_1.add(new DefaultMutableTreeNode("s3"));
						node_1.add(new DefaultMutableTreeNode("s4"));
					add(node_1);
				}
			}
		));
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JTextPane textPane = new JTextPane();
		panel.add(textPane);
		
		JLabel lblNewJgoodiesLabel_1 = DefaultComponentFactory.getInstance().createLabel("Type:   ");
		panel.add(lblNewJgoodiesLabel_1);
		
		JComboBox <SignalType>comboBox = new JComboBox<>();
		comboBox.setModel(new DefaultComboBoxModel<>(SignalType.values()));
		
		panel.add(comboBox);
		
		JLabel lblNewJgoodiesLabel = DefaultComponentFactory.getInstance().createLabel("Name:  ");
		panel.add(lblNewJgoodiesLabel);
		
		JTextPane textPane_1 = new JTextPane();
		panel.add(textPane_1);
		
		JLabel lblNewLabel = new JLabel("Value:   ");
		panel.add(lblNewLabel);
		
		JTextPane textPane_2 = new JTextPane();
		panel.add(textPane_2);
		
		JButton btnNewButton = new JButton("Add");
		
		JButton btnDelete = new JButton("Delete");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnNewButton)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnDelete))
						.addComponent(tree, GroupLayout.PREFERRED_SIZE, 234, GroupLayout.PREFERRED_SIZE))
					.addGap(97)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 489, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(56, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(tree, GroupLayout.PREFERRED_SIZE, 386, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(btnNewButton)
								.addComponent(btnDelete)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(44)
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(19, Short.MAX_VALUE))
		);
		
		JLabel lblAttribute = DefaultComponentFactory.getInstance().createLabel("Attribute");
		panel.add(lblAttribute);
		
		JComboBox<Atributes> comboBox_1 = new JComboBox<>();
		comboBox_1.setModel(new DefaultComboBoxModel<>(Atributes.values()));
		panel.add(comboBox_1);
		contentPane.setLayout(gl_contentPane);
	}
}
